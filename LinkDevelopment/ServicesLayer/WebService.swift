//
//  WebService.swift
//  Ford
//
//  Created by Michael Adliy Nagib on 8/13/16.
//  Copyright © 2016 Michael Adliy Nagib. All rights reserved.
//

import UIKit
import Alamofire

class WebService: NSObject {
    
    class func execute(_ url : String , method: HTTPMethod , parameters : [String : Any]! , header : [String : Any]! , delegate : AnyObject!, callBack : Selector , service : GlobalVariables.Service) {
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    parseData(delegate, callBack: callBack, service: service, dic: JSON as! NSDictionary)
                }
                else {
                    serviceFail(delegate, callBack: callBack, service: service , error: nil)
                }
            case .failure(let error):
                serviceFail(delegate, callBack: callBack, service: service , error: error)
            }
        }
    }
    
    class func parseData(_ delegate : AnyObject!, callBack : Selector , service : GlobalVariables.Service , dic : NSDictionary!) {
        if service == GlobalVariables.Service.login {
            let user = ParseUser.parseData(dictionary: dic)
            delegate.performSelector(inBackground: callBack, with: user)
        }
    }
    
    
    class func serviceFail(_ delegate : AnyObject!, callBack : Selector , service : GlobalVariables.Service , error : Error!) {
        if service == GlobalVariables.Service.login {
        }
    }
    
}

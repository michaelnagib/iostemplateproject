// //  ServiceLayer.swift
//  Ford
//
//  Created by Michael Adliy Nagib on 8/12/16.
//  Copyright © 2016 Michael Adliy Nagib. All rights reserved.
//
import Foundation
import Alamofire

class ServiceLayer: NSObject {
	
	class func login (username : String , password : String , method: HTTPMethod , delegate : AnyObject!, callBack : Selector) {
        // init body request parameters
        let parameters = [GlobalVariables.Login.Username.rawValue : username , GlobalVariables.Login.Password.rawValue : password] as [String : Any]
        
        // init header request parameters
        let header = [:] as [String : Any]
        
        // init url
        let url:String = GlobalVariables.Service.baseUrl.rawValue + GlobalVariables.Service.login.rawValue
        
        // call webservice
        WebService.execute(url , method: method, parameters: parameters ,header: header, delegate: delegate, callBack: callBack, service: GlobalVariables.Service.login)
	}
    
}

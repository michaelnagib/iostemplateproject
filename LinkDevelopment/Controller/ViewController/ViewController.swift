//
//  ViewController.swift
//  LinkDevelopment
//
//  Created by MIS on 4/2/17.
//  Copyright © 2017 MCC. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        ServiceLayer.login(username: "", password: "", method: .post, delegate: self, callBack: #selector(loginSuccess))
    }
    
    func loginSuccess (user : User!) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

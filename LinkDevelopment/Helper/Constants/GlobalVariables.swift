//
//  GlobalVariables.swift
//  Ford
//
//  Created by Michael Adliy Nagib on 8/12/16.
//  Copyright © 2016 Michael Adliy Nagib. All rights reserved.
//

import Foundation
import UIKit

class GlobalVariables: NSObject {
	
    enum Service : String {
        case baseUrl = "WebServices/"
        case login
	}
    
    enum Login : String {
        case Username
        case Password
    }
	
}
